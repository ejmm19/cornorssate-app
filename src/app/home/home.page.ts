import { Component } from '@angular/core';
import { AuthService } from "../service/auth.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public name: string;
  private dataUserId: any;
  msjError = null;
  sendingData = false;
  constructor(public httpClient: HttpClient,private authService: AuthService, private router: Router) {
    this.name = authService.userName;
    // @ts-ignore
    this.dataUserId = this.authService.getDataUser().id;
  }

  private _URL = 'https://schoolapp.digital/api/reportes/';


  goToCuestions(){
    let id = this.dataUserId;
    this.sendingData = true;
    return this.httpClient.get(this._URL+id, {observe: 'response'})
      .subscribe(data => {
        // @ts-ignore
        console.log(data);
        // @ts-ignore
        // this.msjOk = data.body.message;
        if (data.statusText === 'OK' && data.body.canresponse === true){
          this.router.navigate( ['cuestionario']);
        }else{
          // @ts-ignore
          this.msjError = 'No puedes llenar el custionario, deben pasar como mínimo 24 horas para volver a intentar.';

        }
        this.sendingData = false;
      }, error => {
        console.log(error);
        this.sendingData = false;
      });
  }

  salir () {
    this.authService.logoutUser();
    this.router.navigate( ['login']);
  }


}
