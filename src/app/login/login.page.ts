import { Component, OnInit } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  form = {
    document: '',
    password: ''
  };
  docValid: any = '';
  messageDocErr = 'Documento no válido'

  msjErrorLogin = '';
  msjOkLogin = '';

  sendingData = false;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  private _URL = 'https://schoolapp.digital/api/estudents';

  constructor(public httpClient: HttpClient, private authService: AuthService, private router: Router ) {
  }


  ngOnInit() {
  }

  login(){
    this.sendPostRequest(this.form);
  }

  validateDocument(e) {
    // @ts-ignore
    this.msjErrorLogin = '';
    if (this.form.document.length > 0){
      if (this.validateNumbers(this.form.document)){
        this.docValid = true;
      }else{
        this.docValid = false;
      }
    }else{
      this.docValid = '';
    }
  }

  validateNumbers (val) {
    let reg = new RegExp('^[0-9]+$');
    return reg.test(val);
  }

  goToHome(){
    this.router.navigate( ['home']);
  }

  sendPostRequest(dataPost) {
    this.sendingData = true;
    return this.httpClient.post(this._URL, dataPost,{observe: 'response'})
      .subscribe(data => {
        // @ts-ignore
        if (data.body.status === 'OK'){
          // @ts-ignore
          this.msjOkLogin = data.body.message;
          // @ts-ignore
          this.authService.login(data.body.data.name, data.body.data).subscribe(data => {
              this.router.navigate( ['home']);
          });
        }else{
          // @ts-ignore
          this.msjErrorLogin = data.body.message;
        }
        this.sendingData = false;
      }, error => {
        console.log(error);
        this.sendingData = false;
      });
  }

}
