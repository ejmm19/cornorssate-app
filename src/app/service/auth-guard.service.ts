import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService  implements CanActivate{

  constructor(private router: Router, private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.authService.isUserLoggedIn()) {
      this.router.navigate(["login"],{ queryParams: { retUrl: route.url} });
    }
      /*  alert('You are not allowed to view this page. You are redirected to login Page');


        return false;

        //var urlTree = this.router.createUrlTree(['login']);
        //return urlTree;
      }else{
        console.log(this.authService.userName);
      }
  */
    return true;
  }
}
