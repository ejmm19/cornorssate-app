import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isloggedIn: boolean;
  private dataUser: object;
  public userName:string;

  constructor() {
    this.isloggedIn=false;
  }

  login(username: string, data: Object) {


    //Assuming users are provided the correct credentials.
    //In real app you will query the database to verify.
    this.isloggedIn = true;
    this.userName = username;
    this.dataUser = data;
    return of(this.isloggedIn);
  }

  isUserLoggedIn(): boolean {
    return this.isloggedIn;
  }
  getDataUser(): Object{
    return this.dataUser;
  }

  logoutUser(): void{
    this.isloggedIn = false;
  }

}
