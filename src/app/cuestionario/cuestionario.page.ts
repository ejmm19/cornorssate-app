import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import { AuthService } from "../service/auth.service";

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.page.html',
  styleUrls: ['./cuestionario.page.scss'],
})
export class CuestionarioPage implements OnInit {

  form: FormGroup;

  private _URL = 'https://schoolapp.digital/api/reportes';
  private dataUser;
  dataUserId;
  public msjOk = null;
  public msjError = null;

  sendingData = false;

  constructor(fb: FormBuilder, public httpClient: HttpClient, private router: Router, private authService: AuthService) {
    this.form = fb.group({
      temperatura_mayor: ['', Validators.required],
      tos_seca: ['', Validators.required],
      dolor_de_garganta: ['', Validators.required],
      secresion_nasal: ['', Validators.required],
      congestion_nasal: ['', Validators.required],
      malestar_general: ['', Validators.required],
      dificultad_al_respirar: ['', Validators.required],
      perdida_del_olfato: ['', Validators.required],
      perdida_del_gusto: ['', Validators.required],
      dolor_muscular: ['', Validators.required],
      dolor_de_cabeza: ['', Validators.required],
      nauseas: ['', Validators.required],
      vomito: ['', Validators.required],
      diarrea: ['', Validators.required],
      dolor_abdominal_inusual: ['', Validators.required],
      ojos_rojos: ['', Validators.required],
      estornudos: ['', Validators.required],
      brote_salpullido: ['', Validators.required],
      contacto_con_persona_con_covid: ['', Validators.required],
      diagnosticado_con_covid_o_sospecha: ['', Validators.required],
    });
    this.dataUser = this.authService.getDataUser();
    // @ts-ignore
    this.dataUserId = this.authService.getDataUser().id;
  }

  ngOnInit() {
    // @ts-ignore
    console.log(this.authService.getDataUser().id);
  }

  saveData(){
    this.sendingData = true;
    let dataPost = {
      // @ts-ignore
        user_id: this.dataUserId,
        responses : this.form.value
    }
    return this.httpClient.post(this._URL, dataPost,{observe: 'response'})
      .subscribe(data => {
        // @ts-ignore
        if (data.body.status === 'OK'){
          // @ts-ignore
          this.msjOk = data.body.message;
        }else{
          // @ts-ignore
          this.msjError = data.body.message;
          console.log('Error');
        }
        this.sendingData = false;
      }, error => {
        this.sendingData = false;
        console.log(error);
      });
  }

  salir () {
    this.authService.logoutUser();
    this.router.navigate( ['login']);
  }

}
